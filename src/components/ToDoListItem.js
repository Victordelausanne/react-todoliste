import React, {Component} from 'react';

class ToDoListItem extends Component {
  render() {
    const {
      titre,
      ...props
    } = this.props;

    return (
      <div className="ToDoListItem" {...props}>
        <div className="ToDoListItem-titre">{titre}</div>
      </div>
    );
  }
}

export default ToDoListItem;
