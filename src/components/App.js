import React, {Component} from 'react';
import ToDoListItem from './ToDoListItem.js';
import localStorage from 'localStorage';

class App extends Component {
  state = {
    todoList: JSON.parse(localStorage.getItem('todoList')) || [],
  }

  addTodo = (item, callBack) => {
    this.setState(
        {
          todoList: this.state.todoList.concat(item),
        },
        () => {
          localStorage.setItem('todoList', JSON.stringify(this.state.todoList));
          callBack && callBack();
        }
    );
  }

  removeTodo = (item, callBack) => {
    this.setState(
        {
          todoList: this.state.todoList.filter(x => x !== item),
        },
        () => {
          localStorage.setItem('todoList', JSON.stringify(this.state.todoList));
          callBack && callBack();
        }
    );
  }

  render() {
    return (
      <div className='todo__liste__app'>
        <div>
          <h1> Todo Liste </h1>
          <form
            className='form__liste'
            onSubmit={e => {
              e.preventDefault();
              const titreElement = e.target.elements['titre'];
              this.addTodo(
                  {
                    titre: titreElement.value,
                  },
                  () => {
                    titreElement.value = '';
                  }
              );
            }}
          >
            <div className='input__ctnr'>
              <div className="titre__input">
                <input id='titre' />
              </div>
              <div>
                <button type='submit'> + </button>
              </div>
            </div>
          </form>
          <div className="liste">
            {this.state.todoList.map(todo => (
              <ToDoListItem
                key={todo.titre}
                titre={todo.titre}
                onClick={() => this.removeTodo(todo)}
              />
            ))}
          </div>
        </div>
      </div>
    );
  }
}

export default App;
